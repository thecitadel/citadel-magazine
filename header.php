<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
        <title><?php wp_title(); ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="profile" href="https://gmpg.org/xfn/11" />
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div id="page" class="site">

	<a class="skip show-on-focus" href="#content">Skip to content</a>

	<header id="masthead">
		
		<div class="site-branding container">
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
				<?php
				if (is_home() || is_front_page()) :
				?>
				
				<h1>
					<img src="<?php echo get_template_directory_uri() . '/assets/images/The-Citadel-Magazine-Nameplate_White.png' ?>">
				</h1>
				
				<?php else : ?>

				<img src="<?php echo get_template_directory_uri() . '/assets/images/The-Citadel-Magazine-Nameplate_White.png' ?>">

				<?php endif; ?>
			</a>
		</div>

	</header><!-- #masthead -->

	<div id="content" class="site-content">