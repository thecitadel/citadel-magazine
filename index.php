<?php get_header(); ?>

	<section id="primary" class="content-area">

		<main id="main" class="site-main">

			<div class="digital-issues issues-container flex-container flex-center flex-middle flex-wrap">

				<?php

				$args = array(
					'category_name' 	=> 'digital-magazine',
					'post_type' 		=> 'post',
					'post_status'		=> 'publish',
					'posts_per_page'	=> -1,
					'orderby'			=> 'title',
					'order'   			=> 'DESC',
				);

				$the_query = new WP_Query( $args );

				if ( $the_query->have_posts() ) :

					// Load posts loop.
					while ( $the_query->have_posts() ) : $the_query->the_post();
						get_template_part( 'template-parts/content/content', 'magazine' );
					endwhile;

				endif;

				wp_reset_postdata();

				?>

				<button class="issues-prev issue-toggle">
					<p class="prev">Previous Issues</p>
				</button>

			</div>

			<div class="previous-issues issues-container flex-container flex-center flex-middle flex-wrap">

				<?php

				$args = array(
					'category_name' 	=> 'document-magazine',
					'post_type' 		=> 'post',
					'post_status'		=> 'publish',
					'posts_per_page'	=> -1,
					'orderby'			=> 'title',
					'order'   			=> 'DESC',
				);

				$the_query = new WP_Query( $args );

				if ( $the_query->have_posts() ) :

					// Load posts loop.
					while ( $the_query->have_posts() ) : $the_query->the_post();
						get_template_part( 'template-parts/content/content', 'magazine' );
					endwhile;

				endif;

				wp_reset_postdata();

				?>

				<button class="issues-current issue-toggle">
					<p class="prev">Digital Issues</p>
				</button>

			</div>

		</main><!-- .site-main -->

	</section><!-- .content-area -->

<?php get_footer(); ?>