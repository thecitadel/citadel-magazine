jQuery(document).ready(function($) {

	function prevClick() {
		$('.digital-issues').css('left', '-100%');
		$('.previous-issues').css('left', '0');
		$('.issues-prev').css('right', '100%');
		$('.issues-current').css('left', '0');

		var url = window.location.protocol,
			newURL = url + '?p=previous';

		window.history.pushState( { path: newURL }, '', newURL);
	}

	function currentClick() {
		$('.digital-issues').css('left', '0');
		$('.previous-issues').css('left', '100%');
		$('.issues-current').css('left', '100%');
		$('.issues-prev').css('right', '0');

		history.pushState('', document.title, window.location.pathname);
	}

	$('.issues-prev').click(function() {
		prevClick();
	});

	$('.issues-current').click(function() {
		currentClick();
	});

	$('.magazine-item').click(function(e) {

		if ($(this).hasClass('digital-magazine')) {
			e.preventDefault();
			var goTo = $(this).attr('href');

			$(this).addClass('selected');
			$('#masthead').addClass('selected');
			$('.digital-issues').addClass('selected');
			$('.previous-issues').addClass('selected');
			$('.magazine-item:not(.selected)').hide();

			setTimeout(function(){
				window.location = goTo;
			}, 1250);
		}

	});

	if (document.location.search.length) { // if there is a query
		prevClick();
	}

});