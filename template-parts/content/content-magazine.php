<?php

$categories = get_the_category();

?>

<a id="post-<?php the_ID(); ?>" class="magazine-item flex-item<?php echo ' ' . $categories[0]->slug; ?>" href="<?php the_field('link_to_magazine'); ?>">

	<div class="magazine-container">
		
		<div class="cover">

			<?php the_post_thumbnail() ?>

		</div>

		<div class="details">
			<h2><?php the_title(); ?></h2>
			<!-- <button class="site-link">View the magazine</button> -->
		</div>

	</div>

</a>